E3 uwu


Tuto arduino débutant 2 : Controller 
============

**Points abordés :**

- Librairie
- Module
- Moyenne glissante


**Matériel :**
- capteur à ultrason
- plaque de montage facile
- arduino uno
- des câbles
- centrale inertielle MPU 6050
- logiciel arduino
- logiciel python

**Auteurs :**
- Croisette Thomas 16/04/2021

**Intro :**

Ce tuto s’adresse aux débutants en arduino. L’objectif est de maîtriser les fonctionnalités de base de l’arduino pour se lancer dans d’autres projets plus ambitieux.
Pour cela nous allons concevoir un contrôleur physique pour des jeux avec un arduino. 

**Le montage :**
Nous allons réaliser ce montage : 

![montage](Photos/montage.jpg)

Pour s’y retrouver dans ces fils mal rangé il faut :

  - Relier les Vcc aux 5v
  - Relier les Gnd ensemble
  - trig sur Digital 2
  - echo sur Digital 3
  - INT sur Digital 4
  - SDA sur Analogique 4
  - SCL sur Analogique 5

**Le code :**

Nous allons commencer à programmer le capteur à ultrason.
Vous allez réaliser le code tuto_arduino2_code1. Si vous avez suivi e premier tuto, il n’y a pas de grande surprise.
La nouveauté vient de la fonction lire la distance, il s’agit d’un classique pour utiliser le capteur à ultrason. Le fonctionnement est simple, on envoie une impulsion, et on lit le retour. Le temps entre les deux est proportionnel à la distance.

```php

    float lire_distance() {
    // Émission d'un signal de durée 10 microsecondes
    digitalWrite(trigPin, LOW);
    delayMicroseconds(5);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    // Écoute de l'écho 
    duree = pulseIn(echoPin, HIGH);
    // Calcul de la distance
    distance = duree*0.034/2;
    return(distance);
    
    }

```

La distance s’affiche sur le port série.
Je vous propose de valider ce code, et de vous assurer que le montage est fiable et qu’il mesure es bonnes distances.
Maintenant nous allons réaliser le code tuto_arduino2_cod2.
Je vous invite écrire par vous-même ce code, pour vous entrainer avec la syntaxe.


```php

  float moyenne_glissante(float* liste,float nouveau){//On recupère le pointeur vers la liste ("l'adresse de la liste") et la nouvelle valeur
  float moyenne=0;//on creer une variable pour stocker la moyenne
  for (int i=0;i<49;i+=1){//boucle for sur la liste, attention la valeur 49 correspond à la longueur de la liste
   liste[i]=liste[i+1];//on décale chaques termes de la liste
   moyenne+=liste[i];//on additionne pour calculer la moyenne
  }
  liste[49]=nouveau ;//on introduit le dernier éléments
  moyenne+=liste[49];
  moyenne/=50;//on calcul la moyenne
  return(moyenne); 
}

```

La fonction qu’il faut bien comprendre c’est la moyenne glissante. Si vous avez terminé les cours de C, vous comprenez le fonctionnement de la liste. (Sinon, vous pouvez me demander pour avoir plus d’infos). 
La moyenne glissante permet de compenser des mesures fausses, ce qui peut arriver avec le capteur à ultrason. Cependant si la liste est trop grande, on créer une sorte d’inertie, on perd en réactivité de la mesure. Il faut donc choisir la taille de la liste pour lisser les erreurs sans perdre en performance.
Maintenant nous avons un capteur à ultrason qui est parfait pour nos applications. Nous allons coder la centrale inertielle.
Nous allons prendre une feuille de calcul blanche pour cette étape.
Pour utiliser le module « MPU6050 » il faut utiliser une librairie.
Pour cela il faut commencer par l’installer sur votre ordinateur.
Vous devez commencer par telecharger un dossier zip que l’on peut trouver à l’adresse : 
https://github.com/jarzebski/Arduino-MPU6050

(On peut aussi trouver des librairies dans le gestionnaire de bibliothèque dans l’onglet outil).
Ensuite vous devez inclure cette libraire dans le logiciel arduino, pour cela il faut inclure un le fichier zip en suivant le chemin :

![zip1](Photos/zip1.png)

Ensuite vous devez inclure la librairie dans le code. Pour cela on doit le sélectionner dans le menu :

![zip2](Photos/zip2.png)

Et on obtient cette ligne de code :

```php
#include <MPU6050.h>
```

Il faut aussi ajouter cette ligne :

```php
#include <Wire.h>
```
c’est pour la communication entre la carte et le module.

Vous pouvez trouver la suite du code dans les exemple de base (onglet fichier => exemple =>MPU master => gyro pitch roll yaw.
Ou la version commenté sous le nom tuto_arduino2_code3.

```php
#include <MPU6050.h>
#include <Wire.h>
MPU6050 mpu;//creer l'objet mpu


void setup() {
  Serial.begin(9600);//commence la communication série
  while(!mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_2G)) //verifie que le module est bien connecté.
  {  Serial.println("Could not find a valid MPU6050 sensor, check wiring!");
     delay(500); 
  }

  // Calibrate gyroscope. The calibration must be at rest.
  // If you don't want calibrate, comment this line.
  mpu.calibrateGyro();//calibration utile pour 
}
void loop(){
  Vector normGyro = mpu.readNormalizeGyro();//lit la valeur des accelerations
  //Serial.println(rawGyro.ZAxis);
  Serial.print(" Xnorm = ");//affiche les trois valeurs
  Serial.print(normGyro.XAxis);
  Serial.print(" Ynorm = ");
  Serial.print(normGyro.YAxis);
  Serial.print(" Znorm = ");
  Serial.println(normGyro.ZAxis);
  delay(10);
}
```

Executez le programme et regardé le resultat des mouvement sur le moniteur série.
Une fois familliarisé avec nous allons essayer de retrouver l’angle avec les acceleration angulaire. pour cela on va realiser une sorte d’integration.
Le code tuto_arduino2_code4 est juste un résumé des codes precedents que vous pouvez utilser pour faire votre controller personnalisé.
Pour l’exemple je realise un cntroler pour le jeu KSP.
L’idée est que si l’angle est grand alors on appuis sur une touche.
Et on a un programme python qui lit le le port série et appuis sur les bonne touche.
Le choix du programme python pour realiser le clavier virtuel est là pour des raison de sécurité.
Le code python est founie est il est commeté. 
Je vous invite à lire et tester le code tuto_arduino2_code5. changer les valeur pour maitriser le code et faire votre prope controlleur.
Si le programme est un peu capricieux, il faut pas hesiter à debrancher et rebrancher le cable USB.
Pour exemple vous pouvez regarder la video ci jointe.

Vous devez maintenant créer un controller pour le jeu créer par UwU.
Il y a un tuto pour coder le jeu, si vous le voulez.
Vous pouvez créer votre controller personnalisé avec ces deux modules.

