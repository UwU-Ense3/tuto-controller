#include <MPU6050.h>
#include <Wire.h>
MPU6050 mpu;

// Définition des numéros de port
const int trigPin = 2;  // Trigger (emission) sur le pin 2
const int echoPin = 3;  // Echo    (réception) sur le pin 3


// Variables utiles
long duree;   // durée de l'echo
int distance; // distance
long XAxis;
long YAxis;
long ZAxis;
long XAxis10[10];
long YAxis10[10];
long ZAxis10[10];
long distance50[50];  //liste de 10 distances
void setup() {
  pinMode(trigPin, OUTPUT); // Configuration du port du Trigger comme une SORTIE
  pinMode(echoPin, INPUT);  // Configuration du port de l'Echo  comme une ENTREE
  Serial.begin(9600); // Démarrage de la communication série
  while(!mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_2G)) 
  {  Serial.println("Could not find a valid MPU6050 sensor, check wiring!");
     delay(500); 
  }
  mpu.calibrateGyro();



}
void loop() {

  Vector normGyro = mpu.readNormalizeGyro();
  Serial.print(" Xnorm = ");
  Serial.print(moyenne_glissante(XAxis10,10,normGyro.XAxis));
  Serial.print(" Ynorm = ");
  Serial.print(moyenne_glissante(YAxis10,10,normGyro.YAxis));
  Serial.print(" Znorm = ");
  Serial.println(moyenne_glissante(ZAxis10,10,normGyro.ZAxis));//////println 
  Serial.print("Distance : ");
  Serial.print(moyenne_glissante(distance50,50,lire_distance()));
  Serial.println("cm");
 
}

long lire_distance() {
  // Émission d'un signal de durée 10 microsecondes
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Écoute de l'écho 
  duree = pulseIn(echoPin, HIGH);
  // Calcul de la distance
  distance = duree*0.034/2;
  return(distance);
  
}

long moyenne_glissante(long* liste,int len,long nouveau){//nouvelle version de la moyenne glissante avec la longueur de la liste en argument
  long moyenne=0;
  for (int i=0;i<len-1;i+=1){
   liste[i]=liste[i+1];
   moyenne+=liste[i];
  }
  liste[len-1]=nouveau ;
  moyenne+=liste[len-1];
  moyenne/=len;
  return(moyenne); 
}
