#include <MPU6050.h>
#include <Wire.h>
MPU6050 mpu;//creer l'objet mpu


void setup() {
  Serial.begin(9600);//commence la communication série
  while(!mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_2G)) //verifie que le module est bien connecté.
  {  Serial.println("Could not find a valid MPU6050 sensor, check wiring!");
     delay(500); 
  }

  // Calibrate gyroscope. The calibration must be at rest.
  // If you don't want calibrate, comment this line.
  mpu.calibrateGyro();//calibration utile pour 
}
void loop(){
  Vector normGyro = mpu.readNormalizeGyro();//lit la valeur des accelerations
  //Serial.println(rawGyro.ZAxis);
  Serial.print(" Xnorm = ");//affiche les trois valeurs
  Serial.print(normGyro.XAxis);
  Serial.print(" Ynorm = ");
  Serial.print(normGyro.YAxis);
  Serial.print(" Znorm = ");
  Serial.println(normGyro.ZAxis);
  delay(10);
}
