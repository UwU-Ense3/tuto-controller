// Définition des numéros de port
const int trigPin = 2;  // Trigger (emission) sur le pin 2
const int echoPin = 3;  // Echo    (réception) sur le pin 3


// Variables utiles
long duree;   // durée de l'echo
int distance; // distance
float distance10[50];  //liste de 10 distances
void setup() {
pinMode(trigPin, OUTPUT); // Configuration du port du Trigger comme une SORTIE
pinMode(echoPin, INPUT);  // Configuration du port de l'Echo  comme une ENTREE
Serial.begin(9600); // Démarrage de la communication série
}
void loop() {
 // Affichage de la distance dans le Moniteur Série
 Serial.print("Distance : ");
 Serial.print(moyenne_glissante(distance10,lire_distance()));
 Serial.println("cm");
}

float lire_distance() {
  // Émission d'un signal de durée 10 microsecondes
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Écoute de l'écho 
  duree = pulseIn(echoPin, HIGH);
  // Calcul de la distance
  distance = duree*0.034/2;
  return(distance);
  
}

float moyenne_glissante(float* liste,float nouveau){//On recupère le pointeur vers la liste ("l'adresse de la liste") et la nouvelle valeur
  float moyenne=0;//on creer une variable pour stocker la moyenne
  for (int i=0;i<49;i+=1){//boucle for sur la liste, attention la valeur 49 correspond à la longueur de la liste
   liste[i]=liste[i+1];//on décale chaques termes de la liste
   moyenne+=liste[i];//on additionne pour calculer la moyenne
  }
  liste[49]=nouveau ;//on introduit le dernier éléments
  moyenne+=liste[49];
  moyenne/=50;//on calcul la moyenne
  return(moyenne); 
}
